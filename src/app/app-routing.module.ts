import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './components/admin';
import {LoginComponent} from './components/login';
import {AuthGuard} from './components/_guards';
import {AgendaComponent} from './components/agenda/agenda.component';
import {CreateEmploymentComponent} from './components/create-employment/create-employment.component';
import {HomeComponent} from './components/home/home.component';
import {ProfileComponent} from './components/profile/profile.component';
import {EmploymentsComponent} from './components/employments/employments.component';
import {Role} from './models/role';
import {RegisterComponent} from './components/register/register.component';
import {NotificationComponent} from './components/notification/notification.component';
import {UserlistComponent} from './components/admin/userlist/userlist.component';
import {OrganizationlistComponent} from './components/admin/organizationlist/organizationlist.component';
import {ValidateAccountComponent} from './components/validate-account/validate-account.component';
import {EditEmploymentComponent} from './components/edit-employment/edit-employment.component';
import {ManageFilesComponent} from './components/files/manage-files/manage-files.component';
import {AddFileComponent} from './components/files/add-file/add-file.component';
import {CreateOrganisationComponent} from './components/create-organisation/create-organisation.component';
import {ForgotPasswordComponent} from './components/forgot-password/forgot-password.component';
import {ValidatePasswordComponent} from './components/validate-password/validate-password.component';
import {AdminlistComponent} from './components/admin/adminlist/adminlist.component';
import {OrganisationFilesComponent} from './components/files/organisation-files/organisation-files.component';
import {FilesComponent} from './components/files/files.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'notifications',
    component: NotificationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'agenda',
    component: AgendaComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ZZP, Role.ADMIN] }
  },
  {
    path: 'employments',
    component: EmploymentsComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] }
  },
  {
    path: 'employments/:id/edit',
    component: EditEmploymentComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] }
  },
  {
    path: 'files',
    component: FilesComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ZZP, Role.ADMIN] }
  },
  {
    path: 'files',
    canActivate: [AuthGuard],
    data: { roles: [Role.ZZP, Role.ADMIN] },
    children: [
      {
        path: 'employment',
        component: ManageFilesComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.ZZP, Role.ADMIN] }
      },
      {
        path: 'organization',
        component: OrganisationFilesComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.ZZP, Role.ADMIN] }
      },
      {
        path: 'add',
        component: AddFileComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.ADMIN] }
      }
    ]
  },
  // {
  //   path: 'my-employments',
  //   component: MyEmploymentsComponent,
  //   canActivate: [AuthGuard],
  //   data: { roles: [Role.ORGANIZATION] }
  // },
  // {
  //   path: 'user/:id',
  //   component: ViewProfileComponent,
  //   canActivate: [AuthGuard],
  //   data: { roles: [Role.ORGANIZATION] }
  // },
  {
    path: 'create-employment',
    component: CreateEmploymentComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] }
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] }
  },
  {
    path: 'users',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] }
  },
  {
    path: 'users',
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] },
    children: [
      {
        path: 'zzp',
        component: UserlistComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.ADMIN] }
      },
      {
        path: 'admin',
        component: AdminlistComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.ADMIN] }
      }
    ]
  },
  {
    path: 'organizationlist',
    component: OrganizationlistComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] }
  },
  {
    path: 'organization/create',
    component: CreateOrganisationComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.ADMIN] }
  },
  {
    path: 'validate-account',
    component: ValidateAccountComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'reset-password',
    component: ValidatePasswordComponent
  },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
