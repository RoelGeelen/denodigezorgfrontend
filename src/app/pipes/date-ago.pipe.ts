import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateAgo',
  pure: true
})
export class DateAgoPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      const seconds = Math.floor((+new Date() - +new Date(value)) / 1000);
      if (seconds < 29) { // less than 30 seconds ago will show as 'Just now'
        return 'Zojuist';
      }
      const intervals = {
        jaar: 31536000,
        maand: 2592000,
        week: 604800,
        dag: 86400,
        uur: 3600,
        minuut: 60,
        seconde: 1
      };
      let counter;
      for (const i in intervals) {
        counter = Math.floor(seconds / intervals[i]);
        if (counter > 0) {
          if (counter === 1) {
            return counter + ' ' + i + ' geleden'; // singular (1 day ago)
          } else {
            if (i === 'maand') {
              return counter + ' maanden geleden'; // plural (2 days ago)
            }
            if (i === 'week') {
              return counter + ' weken geleden'; // plural (2 days ago)
            }
            if (i === 'dag') {
              return counter + ' dagen geleden'; // plural (2 days ago)
            }
            if (i === 'minuut') {
              return counter + ' minuten geleden'; // plural (2 days ago)
            }
            return counter + ' ' + i + ' geleden'; // plural (2 days ago)
          }
        }
      }
    }
    return value;
  }


}
