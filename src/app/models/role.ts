﻿export enum Role {
  ADMIN = 'ADMIN',
  ZZP = 'ZZP',
  ORGANIZATION = 'ORGANIZATION',
  GUEST = 'GUEST'
}
