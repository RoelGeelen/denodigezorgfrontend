export class Admin {
    id: number;
    fullName: string;
    username: string;
    password: string;
    email: string;
    phone: string;
    pic: string;
    role: string;
    gender: string;
    age: number;
    description: string;
}