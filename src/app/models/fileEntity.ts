import {Organization} from './organization';

export class FileEntity {
  id: number;
  name: string;
  content: string;
  filename: string;
  fileType: string;
  uploadDate: Date;
  header: string;
}
