import {Organization} from './organization';
import {FileEntity} from './fileEntity';

export class OrganizationAndFile {
  organization: Organization;
  file: FileEntity;
}
