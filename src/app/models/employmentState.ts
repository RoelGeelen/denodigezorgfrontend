export enum EmploymentState {
  FILLED = 'FILLED',
  OPEN = 'OPEN',
  EXPIRED = 'EXPIRED',
  CLOSED = 'CLOSED',
  ALL = 'ALL'
}
