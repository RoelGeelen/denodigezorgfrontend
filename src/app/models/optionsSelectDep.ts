import {Department} from './department';

export class OptionsSelectDep {
    value: Department;
    label: string;
}

