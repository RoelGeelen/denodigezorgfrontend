import {Department} from './department';

export class Organization {
    id: number;
    email: string;
    name: string;
    phone: string;
    image: string;
    location: string;
    description: string;
    siteUrl: string;
    departments: Department[];
}
