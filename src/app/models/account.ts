import {Role} from './role';

export class Account {
  id: number;
  email: string;
  name: string;
  password: string;
  phone: string;
  image: string;
  lastLogin: Date;
  enabled: boolean;
  role: Role;
}
