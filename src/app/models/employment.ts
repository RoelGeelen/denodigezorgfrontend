import {Organization} from './organization';
import {Zzp} from './zzp';
import {EmploymentState} from './employmentState';
import {Department} from './department';

export class Employment {
  id: number;
  organization: Organization;
  zzp: Zzp[];
  location: string;
  department: Department;
  description: string;
  educationLevel: string;
  wantedEducationLevel: string;
  function: string;
  extraInfo: string;
  hasWorked: boolean;
  plannedStartDate: Date;
  plannedEndDate: Date;
  state: EmploymentState;

  constructor(description?: string, extraInfo?: string, location?: string, department?: Department, educationLevel?: string, functionType?: string, plannedStartDate?: Date, plannedEndDate?: Date) {
    this.description = description;
    this.extraInfo = extraInfo;
    this.location = location;
    this.department = department;
    this.educationLevel = educationLevel;
    this.function = functionType;
    this.plannedStartDate = plannedStartDate;
    this.plannedEndDate = plannedEndDate;
  }
}
