import {Organization} from './organization';

export class OptionsSelect {
    value: Organization;
    label: string;
}

