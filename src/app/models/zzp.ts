import { Account } from './account';
import {EducationLevel} from './educationLevel';
import {Gender} from "./gender";

export class Zzp {
    id: number;
    account: Account;
    dateOfBirth: string;
    description: string;
    gender: Gender;
    profession: string;
    educationLevel: EducationLevel;
}
