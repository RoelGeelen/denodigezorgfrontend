export enum EducationLevel {
    TWO = '2',
    TWO_PLUS = '2+',
    THREE = '3',
    FOUR = '4',
    FIVE = '5',
    SIX = '6'
}
