export class Department {
  id: number;
  name: string;
  location: string;
  description: string;
}
