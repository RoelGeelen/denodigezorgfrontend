import {CalEmploymentEvent} from './cal-employment';

export interface CalEmploymentEventAction {
  label: string;
  cssClass?: string;
  onClick({ event }: {
    event: CalEmploymentEvent;
  }): any;
}
