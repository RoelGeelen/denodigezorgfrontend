import {EventColor} from 'calendar-utils';
import {CalEmploymentEventAction} from './cal-employmentAction';
import {Zzp} from '../zzp';
import {Organization} from '../organization';
import {EmploymentState} from '../employmentState';
import {Department} from "../department";

export interface CalEmploymentEvent {
  id: number;
  start: Date;
  end?: Date;
  employmentId: number;
  organization: Organization;
  zzp: Zzp[];
  location: string;
  department: Department;
  function: string;
  educationLevel: string;
  description: string;
  extraInfo: string;
  hasWorked: boolean;
  plannedStartDate: Date;
  plannedEndDate: Date;
  state?: EmploymentState;
  title: string;
  color?: EventColor;
  actions?: CalEmploymentEventAction[];
  allDay?: boolean;
  cssClass?: string;
  resizable?: {
    beforeStart?: boolean;
    afterEnd?: boolean;
  };
  draggable?: boolean;
}
