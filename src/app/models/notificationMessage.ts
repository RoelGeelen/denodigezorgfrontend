import { Account } from './account';

export class NotificationMessage {
  id: number;
  message: string;
  account: Account;
  date: Date;
  employmentDate: Date;
  isRead: boolean;

  constructor(message?: string, account?: Account) {
    this.message = message;
    this.account = account;
  }
}
