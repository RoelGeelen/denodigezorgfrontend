import { Role } from './role';

export class Token {
    sub:string;
    email: string;
    exp: number;
    iss:string;
    name:string;
    role:string;
    userId:number;
}