import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  private ipAddress = 'http://212.114.110.113:36969/DeNodigeZorg/';
  // private ipAddress = 'http://localhost:36969/DeNodigeZorg/';
  private employmentUrl = 'employments/';
  private loginUrl = 'authenticate/login/';
  private registerUrl = 'authenticate/register/';
  private organizationUrl = 'organizations/';
  private zzpUrl = 'zzp/';
  private notificationUrl = 'notification/';
  private zzpEdit = 'zzp/edit/';
  private organizationEdit = 'organizations/edit/';
  private validateUrl = 'authenticate/validateAccount/';
  private fileUrl = 'files/';
  private forgotPasswordUrl = 'authenticate/forgot-password/';

  constructor() { }

  getIpAdress() {
    return this.ipAddress;
  }
  getOrganizationEditUrl() {
    return this.ipAddress + this.organizationEdit;
  }

  getZzpEditUrl() {
    return this.ipAddress + this.zzpEdit;
  }

  getEmploymentUrl() {
    return this.ipAddress + this.employmentUrl;
  }

  getLoginUrl() {
    return this.ipAddress + this.loginUrl;
  }

  getRegisterUrl() {
    return this.ipAddress + this.registerUrl;
  }

  getOrganizationUrl() {
    return this.ipAddress + this.organizationUrl;
  }

  getOrganizationsUrl() {
    return this.ipAddress + this.organizationUrl;
  }

  getZzpUrl() {
    return this.ipAddress + this.zzpUrl;
  }

  //getAdminUrl() {
  //  return this.ipAddress + this.adminUrl;
  //}

  getNotificationUrl() {
    return this.ipAddress + this.notificationUrl;
  }

  getValidateUrl() {
    return this.ipAddress + this.validateUrl;
  }

  getFileUrl() {
    return  this.ipAddress + this.fileUrl;
  }

  getForgotPasswordUrl() {
    return  this.ipAddress + this.forgotPasswordUrl;
  }
}
