import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlService } from '../urlService/url.service';
import { Zzp } from 'src/app/models/zzp';
import { Account } from 'src/app/models/account';
import { Observable } from 'rxjs';
import { Organization } from 'src/app/models/organization';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  getAllZzp(): Observable<Zzp[]> {
    return this.http.get<Zzp[]>(this.urlService.getZzpUrl());
  }

  getAllAdmin(): Observable<Account[]> {
    return this.http.get<Account[]>(this.urlService.getIpAdress() + 'authenticate/role/ADMIN');
  }

  getAllOrganizations(): Observable<Organization[]> {
    return this.http.get<Organization[]>(this.urlService.getOrganizationUrl());
  }

  editZzp(zzp: Zzp): Observable<any> {
    return this.http.post<any>(this.urlService.getZzpEditUrl() + zzp.id, zzp);
  }

  editAdmin(admin: Account): Observable<any> {
    return this.http.put<any>(this.urlService.getIpAdress() + 'authenticate/' + admin.id, admin);
  }

  editOrganization(organization: Organization): Observable<any> {
    return this.http.post<any>(this.urlService.getOrganizationEditUrl() + organization.id, organization);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete<any>(this.urlService.getZzpUrl() + id);
  }

  deleteAdmin(id: number): Observable<any> {
    return this.http.delete<any>(this.urlService.getIpAdress() + 'authenticate/' + id);
  }
}
