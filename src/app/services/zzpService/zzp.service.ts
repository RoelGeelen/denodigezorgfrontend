import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from '../urlService/url.service';
import { Employment } from 'src/app/models/employment';
import { Observable } from 'rxjs';
import {CalEmploymentEvent} from '../../models/interfaces/cal-employment';

@Injectable({
  providedIn: 'root'
})
export class ZzpService {
  constructor(private http: HttpClient, private urlService: UrlService) { }

  getZzpEmploymentsAgenda(): Observable<CalEmploymentEvent[]> {
    return this.http.get<CalEmploymentEvent[]>(this.urlService.getEmploymentUrl() + 'zzp');
  }

  getAgendaDay(date: string): Observable<CalEmploymentEvent[]> {
    return this.http.get<CalEmploymentEvent[]>(this.urlService.getEmploymentUrl() + 'day/' + date);
  }

  getAgendaWeek(date: string): Observable<CalEmploymentEvent[]> {
    return this.http.get<CalEmploymentEvent[]>(this.urlService.getEmploymentUrl() + 'week/' + date);
  }

  getAgendaMonth(date: string): Observable<CalEmploymentEvent[]> {
    return this.http.get<CalEmploymentEvent[]>(this.urlService.getEmploymentUrl() + 'month/' + date);
  }

  getZzpEmployments(): Observable<Employment[]> {
    return this.http.get<Employment[]>(this.urlService.getEmploymentUrl());
  }

  getEmploymentsByState(state: string): Observable<Employment[]> {

    if (state === 'ALL') {
      return this.getZzpEmployments();
    } else {
      return this.http.get<Employment[]>(this.urlService.getEmploymentUrl() + 'administrator/state/' + state);
    }
  }

}
