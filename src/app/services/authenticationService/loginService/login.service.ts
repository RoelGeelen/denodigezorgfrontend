import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UrlService } from '../../urlService/url.service';
import { UserCredentials } from 'src/app/models/usercredentials';
import { Token } from '../../../models/token';
import { User } from '../../../models/user';
import * as jwt_decode from 'jwt-decode';
import {Role} from '../../../models/role';

@Injectable({providedIn: 'root'})
export class LoginService {
  userCredentials: UserCredentials = new UserCredentials();
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private urlService: UrlService) {
    this.currentUserSubject = new BehaviorSubject<User>((this.convertTokenToUser()));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  login(username: string, password: string) {
    this.userCredentials.email = username;
    this.userCredentials.password = password;
    return this.http.post<any>(this.urlService.getLoginUrl(), this.userCredentials)
      .pipe(map(data => {
        // login successful if there's a jwt token in the response
        if (data) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', data);
          this.currentUserSubject.next(this.convertTokenToUser());
        }
        return this.convertTokenToUser();
      }));
  }

  convertTokenToUser(): User {
    const user: User = new User();
    if (localStorage.getItem('token')) {
      const token: Token = this.getDecodedAccessToken(localStorage.getItem('token'));
      user.id = token.userId;
      user.role = Role[token.role];
      user.username = token.name;
      user.email = token.email;
      return user;
    }
    return null;
  }

  convertTokenToRole(token: string): Role {
    const decodedToken = this.getDecodedAccessToken(token);

    if (decodedToken.role === Role.ORGANIZATION) {
      return Role.ORGANIZATION;
    } else if (decodedToken.role === Role.ZZP) {
      return Role.ZZP;
    } else {
      return null;
    }
  }

  getDecodedAccessToken(token: string): Token {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) { return null; }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  isTokenExpired(token?: string): boolean {
    if (!token) { token = this.getToken(); }
    if (!token) { return true; }

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) { return false; }
    return !(date.valueOf() > new Date().valueOf());
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
  }
}
