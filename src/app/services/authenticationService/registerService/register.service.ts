import { Injectable } from '@angular/core';
import { Zzp } from 'src/app/models/zzp';
import { Organization } from 'src/app/models/organization';
import { Role } from 'src/app/models/role';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UrlService } from '../../urlService/url.service';
import { Account } from 'src/app/models/account';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  registerAccount(account: Account): Observable<any> {
    return this.http.post<any>(this.urlService.getRegisterUrl(), account);
  }

  registerZzp(zzp: Zzp): Observable<any> {
    return this.http.post<any>(this.urlService.getValidateUrl() + 'accountInformation/' + Role.ZZP, zzp);
  }

  registerOrganization(organization: Organization): Observable<any> {
    return this.http.post<any>(this.urlService.getValidateUrl() + 'accountInformation/' + Role.ORGANIZATION, organization);
  }
}
