import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlService } from '../../urlService/url.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  validateAccountZzp(token: string): Observable<any> {
    return this.http.post<any>(this.urlService.getValidateUrl() + 'token', token);
  }

  validateAccountOrganization(token: string): Observable<any> {
    return this.http.post<any>(this.urlService.getValidateUrl() + 'token', token);
  }
}
