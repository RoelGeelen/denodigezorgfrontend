import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlService} from '../../urlService/url.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  forgotPassword(email: string): Observable<any> {
    return this.http.get<any>(this.urlService.getForgotPasswordUrl() + email);
  }

  resetPassword(token: string, password: string): Observable<any> {
    return this.http.post<any>(this.urlService.getForgotPasswordUrl() + token, password);
  }
}
