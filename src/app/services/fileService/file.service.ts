import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlService} from '../urlService/url.service';
import {Observable} from 'rxjs';
import {FileEntity} from '../../models/fileEntity';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  getFilesEmp(): Observable<FileEntity[]> {
    return this.http.get<FileEntity[]>(this.urlService.getFileUrl() + 'employments');
  }

  getFilesOrg(): Observable<FileEntity[]> {
    return this.http.get<FileEntity[]>(this.urlService.getFileUrl() + 'info');
  }

  addFile(file: FileEntity): Observable<any> {
    return this.http.post<any>(this.urlService.getFileUrl(), file);
  }

  deleteFile(id: number): Observable<any> {
    return this.http.delete<any>(this.urlService.getFileUrl() + id);
  }
}
