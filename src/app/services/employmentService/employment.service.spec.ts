import { TestBed } from '@angular/core/testing';

import { EmploymentserviceService } from './employment.service';

describe('EmploymentserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmploymentserviceService = TestBed.get(EmploymentserviceService);
    expect(service).toBeTruthy();
  });
});
