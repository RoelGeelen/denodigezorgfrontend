import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Employment } from 'src/app/models/employment';
import { UrlService } from '../urlService/url.service';
import {Zzp} from '../../models/zzp';

@Injectable({
  providedIn: 'root'
})
export class EmploymentService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  addEmployment(employment: Employment): Observable<any> {
    return this.http.post<any>(this.urlService.getEmploymentUrl(), employment);
  }

  requestEmployment(employment: Employment): Observable<any> {
    return this.http.post<any>(this.urlService.getEmploymentUrl() + 'request/' + employment.id, employment);
  }

  acceptZzpEmployment(employment: Employment, zzp: Zzp): Observable<any> {
    return this.http.post<any>(this.urlService.getEmploymentUrl() + 'accept/' + employment.id + '/' + zzp.id, employment);
  }

  getEmploymentById(id: number): Observable<Employment> {
    return this.http.get<Employment>(this.urlService.getEmploymentUrl() + id);
  }

  updateEmployment(employment: Employment): Observable<any> {
    return this.http.put<any>(this.urlService.getEmploymentUrl() + employment.id, employment);
  }

  getNewestEmployments(): Observable<Employment[]> {
    return this.http.get<Employment[]>(this.urlService.getEmploymentUrl() + 'newest');
  }

  deleteEmployment(id: number): Observable<any> {
    return this.http.delete<any>(this.urlService.getEmploymentUrl() + id);
  }

  changeWorkedEmployment(id: number, hasWorked: boolean): Observable<any> {
    return this.http.put<any>(this.urlService.getEmploymentUrl() + id + '/has-worked', hasWorked);
  }

  searchEmployments(zzpName: string, orgName: string, date: string, page: number) {
    return this.http.get<any>(this.urlService.getEmploymentUrl() + 'search?zzp=' + zzpName + '&date=' + date + '&organization=' + orgName + '&size=10&page=' + page);
  }
}
