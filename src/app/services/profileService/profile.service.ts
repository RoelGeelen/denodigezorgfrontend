import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlService} from '../urlService/url.service';
import {Observable} from 'rxjs';
import {Employment} from '../../models/employment';
import {Zzp} from '../../models/zzp';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient, private urlService: UrlService) { }


  getZzpProfile(id: number): Observable<Zzp> {
    return this.http.get<Zzp>(this.urlService.getZzpUrl() + id);
  }

  getProfile(): Observable<any> {
    return this.http.get<any>(this.urlService.getIpAdress() + 'authenticate/self');
  }
}
