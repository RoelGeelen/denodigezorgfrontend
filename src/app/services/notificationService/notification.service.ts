import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlService} from '../urlService/url.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {NotificationMessage} from '../../models/notificationMessage';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private currentNotificationAmountSubject: BehaviorSubject<number>;
  public currentNotificationAmount: Observable<number>;

  constructor(private http: HttpClient, private urlService: UrlService) {
    this.currentNotificationAmountSubject = new BehaviorSubject<number>((0));
    this.currentNotificationAmount = this.currentNotificationAmountSubject.asObservable();
  }

  setNotificationAmount() {
    this.http.get<number>(this.urlService.getNotificationUrl() + 'amount').subscribe(value => {
      if (value) {
        this.currentNotificationAmountSubject.next(value);
      }
    });
  }

  getNotifications(): Observable<NotificationMessage[]> {
    return this.http.get<NotificationMessage[]>(this.urlService.getNotificationUrl());
  }

  deleteNotification(id: number): Observable<{}> {
    return this.http.delete(this.urlService.getNotificationUrl() + `?id=${id}`);
  }

  setNotificationsUnread(id: number): Observable<number> {
    return this.http.put<number>(this.urlService.getNotificationUrl() + id + '/unread', null);
  }

  setNotificationsRead(id: number): Observable<number> {
    return this.http.put<number>(this.urlService.getNotificationUrl() + id + '/read', null);
  }
}
