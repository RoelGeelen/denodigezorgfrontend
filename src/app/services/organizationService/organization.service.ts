import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UrlService } from '../urlService/url.service';
import { Employment } from 'src/app/models/employment';
import { Observable } from 'rxjs';
import {Organization} from '../../models/organization';
import {FileEntity} from '../../models/fileEntity';
import {Department} from '../../models/department';
import {OrganizationAndFile} from "../../models/OrganizationAndFile";

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  getOrganizationEmployments(state: string): Observable<Employment[]> {
    if (state === 'ALL') {
      return this.http.get<Employment[]>(this.urlService.getEmploymentUrl() + 'organization');
    } else {
      return this.http.get<Employment[]>(this.urlService.getEmploymentUrl() + 'organization/state/' + state);
    }
  }

  addOrganisation(organisation: OrganizationAndFile): Observable<any> {
    return this.http.post<any>(this.urlService.getOrganizationsUrl(), organisation);
  }

  updateOrganisation(organisation: Organization): Observable<any> {
    return this.http.post<any>(this.urlService.getOrganizationsUrl() + 'edit/' + organisation.id, organisation);
  }

  getOrganizations(): Observable<Organization[]> {
    return this.http.get<Organization[]>(this.urlService.getOrganizationsUrl());
  }

  getOrganizationByEmail(email: string): Observable<Organization[]> {
    return this.http.get<Organization[]>(this.urlService.getOrganizationsUrl() + 'email/');
  }

  addDepartmentToOrganisation(orgId: number, department: Department): Observable<any> {
    return this.http.post<any>(this.urlService.getOrganizationsUrl() + 'departments/' + orgId, department);
  }

  getDepartmentsOfOrganisation(orgId: number): Observable<any> {
    return this.http.get<Department[]>(this.urlService.getOrganizationsUrl() + 'departments/' + orgId);
  }

  deleteOrganization(id: number): Observable<any> {
    return this.http.delete<any>(this.urlService.getOrganizationsUrl() + id);
  }
}
