import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/sitecomponents/nav/nav.component';
import { AdminComponent } from './components/admin';
import { LoginComponent } from './components/login';
import { LoadingComponent } from './components/sitecomponents/loading/loading.component';
import { LoadingOverlayComponent } from './components/sitecomponents/loading-overlay/loading-overlay.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { FooterComponent } from './components/sitecomponents/footer/footer.component';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import { MyEmploymentsComponent } from './components/my-employments/my-employments.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreateEmploymentComponent } from './components/create-employment/create-employment.component';
import {HomeComponent} from './components/home/home.component';
import localeNl from '@angular/common/locales/nl-BE';
import {DatePipe, registerLocaleData} from '@angular/common';
import { ProfileComponent } from './components/profile/profile.component';
import { EmploymentsComponent } from './components/employments/employments.component';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { RegisterComponent } from './components/register/register.component';
import { JwtModule } from '@auth0/angular-jwt';
import { ViewProfileComponent } from './components/view-profile/view-profile.component';
import {JwtInterceptor} from './interceptors/jwt.interceptor';
import {ErrorInterceptor} from './interceptors/error.interceptor';
import {RefreshJwtInterceptor} from './interceptors/refreshToken.interceptor';
import {NotificationsIconComponent} from './components/sitecomponents/notifications-icon/notifications-icon.component';
import { NotificationComponent } from './components/notification/notification.component';
import { UserlistComponent } from './components/admin/userlist/userlist.component';
import { OrganizationlistComponent } from './components/admin/organizationlist/organizationlist.component';
import { ValidateAccountComponent } from './components/validate-account/validate-account.component';
import { EditEmploymentComponent } from './components/edit-employment/edit-employment.component';
import { ManageFilesComponent } from './components/files/manage-files/manage-files.component';
import { AddFileComponent } from './components/files/add-file/add-file.component';
import { CreateOrganisationComponent } from './components/create-organisation/create-organisation.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ValidatePasswordComponent } from './components/validate-password/validate-password.component';
import { AdminlistComponent } from './components/admin/adminlist/adminlist.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { OrganisationFilesComponent } from './components/files/organisation-files/organisation-files.component';
import { FilesComponent } from './components/files/files.component';
import {DateAgoPipe} from './pipes/date-ago.pipe';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';

export function jwtTokenGetter() {
  return localStorage.getItem('token');
}

registerLocaleData(localeNl);

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    AdminComponent,
    LoginComponent,
    LoadingComponent,
    LoadingOverlayComponent,
    AgendaComponent,
    FooterComponent,
    MyEmploymentsComponent,
    CreateEmploymentComponent,
    ProfileComponent,
    EmploymentsComponent,
    RegisterComponent,
    ViewProfileComponent,
    NotificationsIconComponent,
    NotificationComponent,
    UserlistComponent,
    OrganizationlistComponent,
    ValidateAccountComponent,
    EditEmploymentComponent,
    ManageFilesComponent,
    AddFileComponent,
    CreateOrganisationComponent,
    ForgotPasswordComponent,
    ValidatePasswordComponent,
    AdminlistComponent,
    OrganisationFilesComponent,
    FilesComponent,
    DateAgoPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    NgSelectModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxDaterangepickerMd.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: jwtTokenGetter
      }
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: RefreshJwtInterceptor, multi: true },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
