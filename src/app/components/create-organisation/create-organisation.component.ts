import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OrganizationService} from '../../services/organizationService/organization.service';
import {Department} from '../../models/department';
import {OrganizationAndFile} from '../../models/OrganizationAndFile';
import {Organization} from '../../models/organization';
import {FileEntity} from '../../models/fileEntity';

@Component({
  selector: 'app-create-organisation',
  templateUrl: './create-organisation.component.html',
  styleUrls: ['./create-organisation.component.scss']
})
export class CreateOrganisationComponent implements OnInit {
  submitted = false;
  organizationAndFile: OrganizationAndFile = new OrganizationAndFile();
  organizationForm: FormGroup;
  successMessage: string;
  newDepartment = new Department();
  departments: Department[] = [];
  messageType: string;
  isToggleFile = false;
  loadingB = false;
  errorAddDepart = false;

  static IsFile(AC: AbstractControl) {
    const name = AC.get('fileName').value;
    const url = AC.get('fileContent').value;
    const isFile = AC.get('isFile').value;
    if ((url || url !== '') && (!name || name === '') && isFile) {
      AC.get('fileName').setErrors( {IsFile: true} );
    } else {
      return null;
    }
  }

  constructor(private formBuilder: FormBuilder, private organisationService: OrganizationService) {
  }

  ngOnInit() {
    this.organizationAndFile.organization = new Organization();
    this.organizationAndFile.file = new FileEntity();
    this.organizationForm = this.formBuilder.group({
      isFile: [''],
      fileName: [''],
      fileContent: [''],
      image: [''],
      name: ['', Validators.required],
      siteUrl: [''],
      email: [''],
      description: ['', Validators.required],
      location: ['', Validators.required],
      phone: ['', Validators.required],
      newDepartmentInputName: [''],
      newDepartmentInputLocation: [''],
      newDepartmentInputDescription: [''],
    }, {validator: CreateOrganisationComponent.IsFile});
  }

  get f() {
    return this.organizationForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.loadingB = true;
    this.organizationAndFile.organization.departments = this.departments;
    if (this.organizationForm.invalid) {
      this.loadingB = false;
      return;
    }
    this.organisationService.addOrganisation(this.organizationAndFile).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De organisatie is succesvol toegevoegd.');
        this.submitted = false;
        this.organizationForm.reset();
        this.departments = [];
        this.organizationAndFile = new OrganizationAndFile();
        this.organizationAndFile.organization = new Organization();
        this.organizationAndFile.file = new FileEntity();
      } else if (data === 501) {
        this.showMessage('Het email adres van de organisatie is al in gebruik.', true);
      } else {
        this.showMessage('Er is iets mis gegaan bij het opslaan.', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets mis gegaan bij het opslaan.', true);
      this.loadingB = false;
    });
  }

  addDepartment() {
    if (this.newDepartment.name !== undefined && this.newDepartment.name !== '' && this.newDepartment.location !== undefined && this.newDepartment.location !== '') {
      if (this.newDepartment.id !== undefined && this.newDepartment.id !== 0) {
        this.departments[this.departments.findIndex(x => x.id === this.newDepartment.id)] = this.newDepartment;
      } else {
        this.departments.push(this.newDepartment);
      }
      this.errorAddDepart = false;
      this.newDepartment = new Department();
    } else {
      this.errorAddDepart = true;
    }
  }

  editDepartment(department: Department) {
    this.newDepartment = new Department();
    this.newDepartment = department;
  }


  fileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.organizationAndFile.file.filename = file.name;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.organizationAndFile.file.content = reader.result.toString();
      };
    }
  }

  removeDepartment(department: string) {
    this.departments.splice(this.departments.findIndex(x => x.name === department), 1);
  }


  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.organizationAndFile.organization.image = reader.result.toString();
      };
    }
  }
}
