﻿import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {NotificationService} from '../../services/notificationService/notification.service';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: LoginService,
    private notificationService: NotificationService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.convertTokenToUser();
    if (currentUser && !this.authenticationService.isTokenExpired()) {
      // check if route is restricted by role
      if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
        // role not authorised so redirect to home page
        this.router.navigate(['/']);
        return false;
      }
      this.notificationService.setNotificationAmount();
      // authorised so return true
      return true;
    }

    // not logged in so redirect to login page with the return url

    this.authenticationService.logout();
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
