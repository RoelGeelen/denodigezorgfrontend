import {Component, OnInit} from '@angular/core';
import {User} from 'src/app/models/user';
import {Router, ActivatedRoute} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from 'src/app/services/authenticationService/loginService/login.service';
import {ValidateService} from 'src/app/services/authenticationService/validateService/validate.service';
import {Role} from 'src/app/models/role';
import {Organization} from 'src/app/models/organization';
import {Zzp} from 'src/app/models/zzp';
import {EducationLevel} from 'src/app/models/educationLevel';
import {Gender} from 'src/app/models/gender';
import {RegisterService} from 'src/app/services/authenticationService/registerService/register.service';
import {delay} from 'rxjs/operators';
import {strictEqual} from 'assert';

@Component({
  selector: 'app-validate-account',
  templateUrl: './validate-account.component.html',
  styleUrls: ['./validate-account.component.scss']
})
export class ValidateAccountComponent implements OnInit {

  constructor(
    private router: Router,
    private validateService: ValidateService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authenticationService: LoginService,
    private registerService: RegisterService
  ) {
  }

  private currentTokenRole: Role;
  registerFormZZP: FormGroup;
  educationLevelsPlaceholder$: string[] = ['2', '2+', '3', '4', '5', '6'];
  selectedEducationLevel = '2';
  selectedGender = 'Man';
  zzp: Zzp = new Zzp();
  messageType: string;
  successMessage: string;
  loading = false;
  submitted = false;

  static MatchPassword(AC: AbstractControl) {
    const password = AC.get('pass1').value; // to get value in input tag
    const confirmPassword = AC.get('pass2').value; // to get value in input tag
    if (password !== confirmPassword) {
      AC.get('pass2').setErrors( {MatchPassword: true} );
    } else {
      return null;
    }
  }

  ngOnInit() {
    if (this.route.snapshot.queryParams.token &&
      this.authenticationService.getDecodedAccessToken(this.route.snapshot.queryParams.token) &&
      this.authenticationService.convertTokenToRole(this.route.snapshot.queryParams.token) === Role.ZZP &&
      !this.authenticationService.isTokenExpired(this.route.snapshot.queryParams.token)) {
      this.validateService.validateAccountZzp(this.route.snapshot.queryParams.token).subscribe((data) => {
        if (data !== null) {
          this.zzp = data;
          this.currentTokenRole = Role.ZZP;

          this.registerFormZZP = this.formBuilder.group({
            image: [''],
            email: ['', Validators.required],
            name: ['', Validators.required],
            description: [''],
            phone: ['', Validators.required],
            pass1: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            pass2: ['', Validators.required],
            age: ['', Validators.required],
            gender: ['', Validators.required],
            profession: ['', Validators.required],
            educationLevel: ['', Validators.required]
          }, {validator: ValidateAccountComponent.MatchPassword});
        } else {
          this.router.navigate(['/login'], { queryParams: { message: 'Er is iets mis gegaan met je validatie.', error: 'true' } });
        }
      }, error => {
        this.router.navigate(['/login'], { queryParams: { message: 'Valideer sessie is niet meer geldig.', error: 'true' } });
      });
    } else {
      this.router.navigate(['/login'], { queryParams: { message: 'Valideer sessie is niet meer geldig.', error: 'true' } });
    }
  }

  get f() {
    return this.registerFormZZP.controls;
  }


  onSubmitZzp() {
    this.submitted = true;
    this.loading = true;

    if (this.registerFormZZP.invalid) {
      this.loading = false;
      return;
    }

    switch (this.selectedEducationLevel) {
      case '2': {
        this.zzp.educationLevel = EducationLevel.TWO;
        break;
      }
      case '2+': {
        this.zzp.educationLevel = EducationLevel.TWO_PLUS;
        break;
      }
      case '3': {
        this.zzp.educationLevel = EducationLevel.THREE;
        break;
      }
      case '4': {
        this.zzp.educationLevel = EducationLevel.FOUR;
        break;
      }
      case '5': {
        this.zzp.educationLevel = EducationLevel.FIVE;
        break;
      }
      case '6': {
        this.zzp.educationLevel = EducationLevel.SIX;
        break;
      }
    }

    switch (this.selectedGender) {
      case 'Man': {
        this.zzp.gender = Gender.MALE;
        break;
      }
      case 'Vrouw': {
        this.zzp.gender = Gender.FEMALE;
        break;
      }
    }

    this.registerService.registerZzp(this.zzp).subscribe(data => {
      if (data === 'Account activated') {
        this.loading = false;
        this.router.navigate(['/login'], { queryParams: { message: 'Uw account is geactiveerd, u kunt nu inloggen.' } });
      }
    }, error => {
      this.showMessage('Er is iets mis gegaan bij het opslaan, Probeer het later opnieuw of neem contact op.'); // error path
      this.loading = false;
    });
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.zzp.account.image = reader.result.toString();

      };
    }
  }

}
