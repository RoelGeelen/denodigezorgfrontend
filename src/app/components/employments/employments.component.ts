import {Component, HostListener, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Employment} from '../../models/employment';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ZzpService} from '../../services/zzpService/zzp.service';
import {EmploymentService} from '../../services/employmentService/employment.service';
import {Observable, Subject} from 'rxjs';
import {EmploymentState} from '../../models/employmentState';
import {User} from '../../models/user';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {Role} from '../../models/role';
import {ActivatedRoute} from '@angular/router';
import {EducationLevel} from '../../models/educationLevel';
import {LocaleConfig} from 'ngx-daterangepicker-material';
import * as moment from 'moment';
import * as localization from 'moment/locale/nl';

moment.locale('nl', localization);

@Component({
  selector: 'app-employments',
  templateUrl: './employments.component.html',
  styleUrls: ['./employments.component.scss'],
})
export class EmploymentsComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  modalData: {
    event: Employment;
  };


  locale: LocaleConfig = {
    applyLabel: 'Appliquer',
    customRangeLabel: ' - ',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek(),
  };
  currentUser: User = new User();
  view: EmploymentState = EmploymentState.ALL;
  EmploymentState = EmploymentState;
  loadingB = false;
  loadingMore = false;
  successMessage: string;
  obEmployment$: Observable<Employment[]>;
  employments: Employment[] = [];
  messageType: string;
  searchZzpName = '';
  searchOrgName = '';
  searchDate = '';
  timesLoaded = 1;

  constructor(private modal: NgbModal,
              private route: ActivatedRoute,
              private zzpService: ZzpService,
              private employmentService: EmploymentService,
              private authenticationService: LoginService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
    this.fetchEmployments();
    if (this.route.snapshot.queryParams.message) {
      this.showMessage(this.route.snapshot.queryParams.message);
    }
  }

  fetchEmployments() {
    this.obEmployment$ = this.employmentService.searchEmployments('', '', '', 0);
    this.obEmployment$.subscribe((data) => {
      data.forEach((value: Employment) => {
        value.plannedStartDate = new Date(value.plannedStartDate);
        value.plannedEndDate = new Date(value.plannedEndDate);
        value.educationLevel = EducationLevel[value.educationLevel];
      });
      this.employments = data;
    }, error => {
      this.showMessage('Kon diensten niet ophalen', true);
    });
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  oModal(event: Employment): void {
    this.modalData = {event};
    this.modal.open(this.modalContent, {size: 'lg'});
  }

  signUp(employment: Employment) {
    this.loadingB = true;
    this.employmentService.requestEmployment(employment).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Je bent ingeschreven bij deze dienst!');
        this.modal.dismissAll();
        this.removeFromList(employment);

      } else {
        this.showMessage('Er is iets fout gegaan bij het inschrijven.', true);
      }
      this.loadingB = false;
    });
  }

  removeFromList(employment: Employment) {
    const index: number = this.employments.indexOf(employment);
    if (index !== -1) {
      this.employments.splice(index, 1);
    }
  }

  delete(employment: Employment) {
    this.loadingB = true;
    this.employmentService.deleteEmployment(employment.id).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De dienst is verwijderd');
        this.removeFromList(employment);
        this.modal.dismissAll();
      } else {
        this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Kon dienst niet verwijderen', true);
      this.loadingB = false;
    });
  }

  search() {
    this.timesLoaded = 1;
    this.obEmployment$ = this.employmentService.searchEmployments(this.searchZzpName, this.searchOrgName, this.searchDate, 0);
    this.obEmployment$.subscribe(data => {
      data.forEach((value: Employment) => {
        value.plannedStartDate = new Date(value.plannedStartDate);
        value.plannedEndDate = new Date(value.plannedEndDate);
        value.educationLevel = EducationLevel[value.educationLevel];
      });
      this.employments = data;
    }, error => {
      this.showMessage('Kon diensten niet ophalen', true);
    });
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    // In chrome and some browser scroll is given to body tag
    const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    const max = document.documentElement.scrollHeight;
    // pos/max will give you the distance between scroll bottom and and bottom of screen in percentage.
    if (pos >= max) {
      this.loadMore();
    }
  }

  loadMore() {
    this.loadingMore = true;
    this.employmentService.searchEmployments(this.searchZzpName, this.searchOrgName, this.searchDate, this.timesLoaded).subscribe(data => {
      data.forEach((value: Employment) => {
        value.plannedStartDate = new Date(value.plannedStartDate);
        value.plannedEndDate = new Date(value.plannedEndDate);
        value.educationLevel = EducationLevel[value.educationLevel];
        this.employments.push(value);
      });
      this.loadingMore = false;
    }, error => {
      this.loadingMore = false;
      this.showMessage('Kon diensten niet ophalen', true);
    });
    this.timesLoaded++;
  }

}
