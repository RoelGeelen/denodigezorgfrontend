﻿import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {NotificationService} from "../../services/notificationService/notification.service";

@Component({templateUrl: 'login.component.html', styleUrls: ['./login.component.scss']})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl = '';
  error = '';
  message: string;
  messageType = 'success';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private loginService: LoginService,
    private notificationService: NotificationService,
  ) {
    // redirect to home if already logged in
    if (this.loginService.convertTokenToUser()) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';

    if (this.route.snapshot.queryParams.message) {
      this.message = this.route.snapshot.queryParams.message;
    }
    if (this.route.snapshot.queryParams.error) {
      this.messageType = 'danger';
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.loginService.login(this.f.username.value, this.f.password.value).subscribe(data => {
        this.loading = false;
        if (data.id != null) {
          this.notificationService.setNotificationAmount();
          this.router.navigate([this.returnUrl]);
        }
        return true;
      }, error => {
        console.log(error);
        this.error = 'Naam en/of wachtwoord klopt niet!'; // error path
        this.loading = false;
      }
    );
  }
}
