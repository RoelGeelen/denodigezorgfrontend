import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { LoginService } from '../../../services/authenticationService/loginService/login.service'
import {User} from '../../../models/user';
import {Role} from '../../../models/role';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  title = 'De nodige zorg';
  currentUser: User = new User();
  show: any;

  constructor(
    private router: Router,
    private authenticationService: LoginService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }

  get isZorg() {
    return this.currentUser && this.currentUser.role === Role.ORGANIZATION;
  }

  get isZzp() {
    return this.currentUser && this.currentUser.role === Role.ZZP;
  }

  get isUser() {
    return this.currentUser;
  }


  logout() {
    this.currentUser = null;
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
  }
}
