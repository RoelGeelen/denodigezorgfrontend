import { Component, OnInit } from '@angular/core';
import {NotificationService} from '../../../services/notificationService/notification.service';
import {tick} from '@angular/core/testing';

@Component({
  selector: 'app-notifications-icon',
  templateUrl: './notifications-icon.component.html',
  styleUrls: ['./notifications-icon.component.scss']
})
export class NotificationsIconComponent implements OnInit {
  notificationCount = 0;

  constructor(private notificationService: NotificationService) {
    this.notificationService.currentNotificationAmount.subscribe(value => {
      this.notificationCount = value;
    });
  }

  ngOnInit() {
    this.notificationService.setNotificationAmount();
  }
}
