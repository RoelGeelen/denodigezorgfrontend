import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Employment} from '../../models/employment';
import {Zzp} from '../../models/zzp';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrganizationService} from '../../services/organizationService/organization.service';
import {EmploymentService} from '../../services/employmentService/employment.service';
import {Observable} from 'rxjs';
import {EmploymentState} from '../../models/employmentState';

@Component({
  selector: 'app-my-employments',
  templateUrl: './my-employments.component.html',
  styleUrls: ['./my-employments.component.scss']
})
export class MyEmploymentsComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  modalData: {
    event: Employment;
  };

  view: EmploymentState = EmploymentState.ALL;
  EmploymentState = EmploymentState;
  messageType: string;
  successMessage: string;
  obEmployment$: Observable<Employment[]>;
  employments: Employment[] = [];


  constructor(private modal: NgbModal, private organizationService: OrganizationService, private employmentService: EmploymentService) { }

  ngOnInit() {
    this.fetchEmployments();
  }

  fetchEmployments() {
    this.obEmployment$ = this.organizationService.getOrganizationEmployments(this.view);
    this.obEmployment$.subscribe((data) => {
      this.employments = data;
    }, error => {
      this.showMessage('Kon diensten niet ophalen', true);
    });
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  acceptZzp(employment: Employment, zzp: Zzp) {
    this.employmentService.acceptZzpEmployment(employment, zzp).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De persoon is geaccepteerd bij de dienst.');
      } else {
        this.showMessage('Er is iets fout gegaan bij het accepteren.', true);
      }
    });
  }

  oModal(event: Employment): void {
    this.modalData = { event};
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  delete(employment: Employment) {
    this.employmentService.deleteEmployment(employment.id).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De dienst is verwijderd');
        const index: number = this.employments.indexOf(employment);
        if (index !== -1) {
          this.employments.splice(index, 1);
        }
        this.modal.dismissAll();
      } else {
        this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      }
    });
  }

}
