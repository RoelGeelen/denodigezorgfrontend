import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ValidateService} from '../../services/authenticationService/validateService/validate.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {PasswordService} from '../../services/authenticationService/passwordService/password.service';

@Component({
  selector: 'app-validate-password',
  templateUrl: './validate-password.component.html',
  styleUrls: ['./validate-password.component.scss']
})
export class ValidatePasswordComponent implements OnInit {
  passwordForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  messageType: string;
  successMessage: string;

  static MatchPassword(AC: AbstractControl) {
    const password = AC.get('pass1').value; // to get value in input tag
    const confirmPassword = AC.get('pass2').value; // to get value in input tag
    if (password !== confirmPassword) {
      AC.get('pass2').setErrors( {MatchPassword: true} );
    } else {
      return null;
    }
  }

  constructor(private router: Router,
              private validateService: ValidateService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private authenticationService: LoginService,
              private passwordService: PasswordService) { }

  ngOnInit() {
    if (this.route.snapshot.queryParams.token &&
      this.authenticationService.getDecodedAccessToken(this.route.snapshot.queryParams.token) &&
      !this.authenticationService.isTokenExpired(this.route.snapshot.queryParams.token)) {

      this.passwordForm = this.formBuilder.group({
        pass1: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
        pass2: ['', Validators.required]
      }, {validator: ValidatePasswordComponent.MatchPassword});
    } else {
      this.router.navigate(['/login'], { queryParams: { message: 'De url is niet geldig.', error: 'true' } });
    }
  }

  get f() {
    return this.passwordForm.controls;
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if (this.passwordForm.invalid) {
      this.loading = false;
      return;
    }

    this.passwordService.resetPassword(this.route.snapshot.queryParams.token, this.f.pass1.value).subscribe(data => {
        this.loading = false;
        if (data === 200) {
          this.router.navigate(['/login'], { queryParams: { message: 'je nieuwe wachtwoord is succesvol opgeslagen.' } });
        } else {
          this.showMessage('Er is iets fout gegaan, probeer het later opnieuw of neem contact met ons op.', true);
        }
      }, error => {
        this.showMessage('Er is iets fout gegaan, probeer het later opnieuw of neem contact met ons op.', true);
        this.loading = false;
      }
    );
  }
}
