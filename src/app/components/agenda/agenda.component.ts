import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {format, isSameDay, isSameMonth, startOfMonth, startOfWeek} from 'date-fns';
import {Observable, Subject} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CalendarDateFormatter, CalendarView} from 'angular-calendar';
import {CalEmploymentEvent} from '../../models/interfaces/cal-employment';
import {CalEmploymentEventAction} from '../../models/interfaces/cal-employmentAction';

import {User} from '../../models/user';
import {ZzpService} from '../../services/zzpService/zzp.service';
import {CustomDateFormatter} from './custom-date-formatter.provider';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {Role} from '../../models/role';
import {DatePipe} from '@angular/common';
import {EducationLevel} from '../../models/educationLevel';
import {EmploymentService} from '../../services/employmentService/employment.service';
import {Employment} from "../../models/employment";
import {el} from "@angular/platform-browser/testing/src/browser_util";


const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  green: {
    primary: '#155724',
    secondary: '#d4edda'
  },
  gray: {
    primary: '#1b1e21',
    secondary: '#d6d8d9'
  },
};

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss'],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ]
})
export class AgendaComponent implements OnInit {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  locale = 'nl-BE';

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  successMessage: string;
  messageType: string;
  totalWorked = 0;
  totalNotWorked = 0;
  modalData: {
    action: string;
    event: CalEmploymentEvent;
  };

  actions: CalEmploymentEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({event}: { event: CalEmploymentEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({event}: { event: CalEmploymentEvent }): void => {
        this.employments = this.employments.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  employments: CalEmploymentEvent[] = [];
  employments$: Observable<CalEmploymentEvent[]>;

  activeDayIsOpen = false;
  loadingB = false;

  currentUser: User;


  constructor(private modal: NgbModal, private employmentService: EmploymentService, private authenticationService: LoginService, private zzpService: ZzpService, public datepipe: DatePipe) {
    this.currentUser = this.authenticationService.convertTokenToUser();
  }

  ngOnInit(): void {
    this.fetchAgenda();

  }

  fetchAgenda() {
    this.employments = [];
    const getData: any = {
      month: this.zzpService.getAgendaMonth(format(startOfMonth(this.viewDate), 'DD-MM-YYYY')),
      week: this.zzpService.getAgendaWeek(format(startOfWeek(this.viewDate), 'DD-MM-YYYY')),
      day: this.zzpService.getAgendaDay(format(this.viewDate, 'DD-MM-YYYY'))
    }[this.view];
    this.employments$ = getData;
    this.employments$.subscribe((data) => {
      this.totalNotWorked = 0;
      this.totalWorked = 0;
      console.log(data);
      this.employments = data;
      for (const employment of this.employments) {
        employment.start = new Date(employment.plannedStartDate);
        employment.end = new Date(employment.plannedEndDate);
        if (this.isAdmin) {
          employment.title = employment.zzp[0].account ? employment.zzp[0].account.name : 'Verwijderde Zzp';
        } else {
          employment.title = employment.department.name + ', ' + this.datepipe.transform(employment.plannedStartDate, 'HH:mm') + ' tot, ' + this.datepipe.transform(employment.plannedEndDate, 'HH:mm');
        }
        if (employment.hasWorked) {
          employment.color = colors.green;
          this.totalWorked++;
        } else {
          employment.color = colors.red;
          this.totalNotWorked++;
        }
        employment.educationLevel = EducationLevel[employment.educationLevel];
      }
      this.refresh.next();
    }, error => {
      this.showMessage('Kon agenda niet ophalen', true);
    });
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  dayClicked({date, events}: { date: Date; events: CalEmploymentEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  handleEvent(action: string, event: CalEmploymentEvent): void {
    this.modalData = {event, action};
    this.modal.open(this.modalContent, {size: 'lg'});
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }

  removeFromList(employment: CalEmploymentEvent) {
    const index: number = this.employments.indexOf(employment);
    if (index !== -1) {
      this.employments.splice(index, 1);
    }
  }

  changeWorked(employment: CalEmploymentEvent) {
    console.log(employment);
    this.loadingB = true;
    this.employmentService.changeWorkedEmployment(employment.id, !employment.hasWorked).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Gewerkt status aangepast!');
        employment.hasWorked = !employment.hasWorked;
        if (employment.hasWorked) {
          employment.color = colors.green;
          this.totalWorked++;
        } else {
          employment.color = colors.red;
          this.totalNotWorked++;
        }
        this.modal.dismissAll();
      } else {
        this.showMessage('Er is iets is mis gegaan met het aanpassen!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Kon dienst niet aanpassen', true);
      this.loadingB = false;
    });
  }

  delete(employment: CalEmploymentEvent) {
    this.loadingB = true;
    this.employmentService.deleteEmployment(employment.id).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De dienst is verwijderd');
        this.removeFromList(employment);
        this.modal.dismissAll();
      } else {
        this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Kon dienst niet verwijderen', true);
      this.loadingB = false;
    });
  }
}

