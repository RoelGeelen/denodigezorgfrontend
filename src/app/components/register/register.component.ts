import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {RegisterService} from '../../services/authenticationService/registerService/register.service';
import {Account} from 'src/app/models/account';
import {Role} from 'src/app/models/role';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  acc: Account = new Account();
  loading = false;
  successMessage: string;
  messageType: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private registerService: RegisterService) {
  }

  ngOnInit() {
    if (this.route.snapshot.queryParams.role) {
      if (this.route.snapshot.queryParams.role === 'ADMIN' || this.route.snapshot.queryParams.role === 'ZZP') {
        this.acc.role = this.route.snapshot.queryParams.role;
      }
    }
    this.registerForm = this.formBuilder.group({
      role: ['', Validators.required],
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required]
    });
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    // this.acc.role = Role.ZZP;

    this.loading = true;
    this.registerService.registerAccount(this.acc).subscribe(data => {
      if (data === 200) {
        this.submitted = false;
        this.showMessage('Nieuw Account aangemaakt!');
        this.registerForm.reset();
      } else {
        this.showMessage('Er is iets mis gegaan met het aanmaken van het account!', true);
      }
      this.loading = false;
    }, error => {
      this.showMessage('Er is iets mis gegaan met het aanmaken van het account!', true); // error path
      this.loading = false;
    });
  }
}
