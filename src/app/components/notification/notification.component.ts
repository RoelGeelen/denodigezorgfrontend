import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../services/notificationService/notification.service';
import {NotificationMessage} from '../../models/notificationMessage';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  notifications: NotificationMessage[] = [];
  obNotifications$: Observable<NotificationMessage[]>;
  messageType: string;
  successMessage: string;

  constructor(private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.notificationService.setNotificationAmount();
    this.obNotifications$ = this.notificationService.getNotifications();
    this.obNotifications$.subscribe((data) => {
      console.log(data);
      data.forEach((value: NotificationMessage) => {
        value.date = new Date(value.date);
      });
      this.notifications = data;
    }, error => {
      this.showMessage('Kon notificaties niet ophalen', true);
    });
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  delete(notification: NotificationMessage) {
    this.notificationService.deleteNotification(notification.id).subscribe((data) => {
      if (data === 200) {
        const index: number = this.notifications.indexOf(notification);
        if (index !== -1) {
          this.notifications.splice(index, 1);
        }
        this.notificationService.setNotificationAmount();
      }
    });
  }

  read(notification: NotificationMessage) {
    this.notificationService.setNotificationsRead(notification.id).subscribe((data) => {
      if (data === 200) {
        notification.isRead = true;
        this.notificationService.setNotificationAmount();
      }
    });
  }
  unread(notification: NotificationMessage) {
    this.notificationService.setNotificationsUnread(notification.id).subscribe((data) => {
      if (data === 200) {
        notification.isRead = false;
        this.notificationService.setNotificationAmount();
      }
    });
  }
}
