import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Employment} from '../../models/employment';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {EmploymentService} from '../../services/employmentService/employment.service';
import {Department} from '../../models/department';
import {OrganizationService} from '../../services/organizationService/organization.service';
import {Organization} from '../../models/organization';
import {OptionsSelect} from '../../models/optionsSelect';
import {OptionsSelectDep} from '../../models/optionsSelectDep';
import {DaterangepickerDirective, LocaleConfig} from 'ngx-daterangepicker-material';
import * as moment from 'moment';
import * as localization from 'moment/locale/nl';
moment.locale('nl', localization);

@Component({
  selector: 'app-edit-employment',
  templateUrl: './edit-employment.component.html',
  styleUrls: ['./edit-employment.component.scss'],
})
export class EditEmploymentComponent implements OnInit {
  @ViewChild(DaterangepickerDirective, {  })
  daterangepicker: DaterangepickerDirective;
  locale: LocaleConfig = {
    format: 'YYYY-MM-DDTHH:mm:ss.SSSSZ',
    displayFormat: 'DD MMMM YYYY HH:mm',
    separator: ' Tot ',
    cancelLabel: 'Annuleer',
    applyLabel: 'Oke',
    customRangeLabel: ' - ',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek(),
  };
  opens = 'right';
  drops = 'up';
  dateTimeRange = { startDate: moment(), endDate: moment() };

  successMessage: string;
  messageType: string;
  loadingB = false;
  dateTimeRangeError = '';
  selectOptionsOrg: OptionsSelect[] = [];
  selectOptionsDep: OptionsSelectDep[] = [];
  obEmployment$: Observable<Employment>;
  employment: Employment = new Employment();
  departments: Department[] = [];
  obOrganization$: Observable<Organization[]>;
  organizations: Organization[] = [];
  submitted = false;
  createEmploymentForm: FormGroup;

  selectedOrg = null;
  selectedDep = null;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private employmentService: EmploymentService, private organizationService: OrganizationService) { }

  ngOnInit() {

    this.employment.organization = new Organization();
    this.employment.department = new Department();
    this.createEmploymentForm = this.formBuilder.group({
      organization: ['', Validators.required],
      zzp: new FormControl({value: '', disabled: true}),
      description: [''],
      function: [''],
      department: ['', Validators.required],
      educationLevel: [''],
      extraInfo: [''],
      location: ['', Validators.required],
      rangeDate: ['']
      // plannedStartDate: [''],
      // plannedEndDate: ['']
    });
    this.obEmployment$ = this.employmentService.getEmploymentById(+this.route.snapshot.paramMap.get('id'));
    this.obEmployment$.subscribe((data) => {
      this.employment = data;
      console.log(this.employment);
      this.dateTimeRange.startDate = moment(this.employment.plannedStartDate);
      this.dateTimeRange.endDate = moment(this.employment.plannedEndDate);
      // this.organizationService.getDepartmentsOfOrganisation(this.employment.organization.id).subscribe((departments) => {
      //  this.departments = departments;
      // }, error => {
      //  this.showMessage('Kon Afdelingen niet ophalen', true);
      // });
      this.loadingB = true;
      this.obOrganization$ = this.organizationService.getOrganizations();
      this.obOrganization$.subscribe((data1) => {
        this.loadingB = false;
        data1.forEach(value => {
          const selectOption: OptionsSelect = new OptionsSelect();
          selectOption.value = value;
          selectOption.label = value.name;
          this.selectOptionsOrg.push(selectOption);
        });
        this.selectedOrg = this.selectOptionsOrg[this.selectOptionsOrg.findIndex(x => x.value.id === this.employment.organization.id)];
        console.log(this.selectedOrg);
        this.selectOptionsDep = [];
        this.selectedOrg.value.departments.forEach(value => {
          const selectOption: OptionsSelectDep = new OptionsSelectDep();
          selectOption.value = value;
          selectOption.label = value.name;
          this.selectOptionsDep.push(selectOption);
        });
        this.selectedDep = this.selectOptionsDep[this.selectOptionsDep.findIndex(x => x.value.id === this.employment.department.id)];
      }, error => {
        this.loadingB = false;
        this.showMessage('Kon organisaties niet ophalen', true);
      });

    }, error => {
      this.showMessage('Kon dienst niet ophalen', true);
    });
  }

  get f() { return this.createEmploymentForm.controls; }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }


  departmentChanged(department: Department) {
    console.log(department);
    if (department) {
      this.employment.department = department;
      this.employment.location = department.location;
    }
  }

  organizationChanged(organization: Organization) {
    console.log(organization);
    if (organization.id) {
      this.selectOptionsDep = [];
      organization.departments.forEach(value => {
        const selectOption: OptionsSelectDep = new OptionsSelectDep();
        selectOption.value = value;
        selectOption.label = value.name;
        this.selectOptionsDep.push(selectOption);
      });
      this.employment.organization = organization;
    }
  }

  onSubmit() {

    this.submitted = true;
    this.loadingB = true;
    this.dateTimeRangeError = '';
    if (this.dateTimeRange.startDate == null || this.dateTimeRange.endDate == null) {
      this.dateTimeRangeError = 'Start of eindtijd niet geselecteerd';
      this.loadingB = false;
      return;
    }

    this.employment.plannedStartDate = this.dateTimeRange.startDate.toDate();
    this.employment.plannedEndDate = this.dateTimeRange.endDate.toDate();

    if (this.createEmploymentForm.invalid) {
      this.loadingB = false;
      return;
    }
    console.log(this.employment);

    this.employmentService.updateEmployment(this.employment).subscribe((data) => {
      console.log(data);
      if (data === 200) {
        this.router.navigate(['/employments'], { queryParams: { message: 'De dienst is succesvol aangepast.' } });
      } else {
        this.showMessage('Er is iets mis gegaan bij het aanpassen van de dienst.', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets mis gegaan bij het aanpassen van de dienst.', true);
      this.loadingB = false;
    });
  }

}
