import {Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs';
import {Organization} from 'src/app/models/organization';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from 'src/app/services/userService/user.service';
import {OrganizationService} from '../../../services/organizationService/organization.service';
import {Department} from '../../../models/department';
import {Role} from '../../../models/role';
import {LoginService} from '../../../services/authenticationService/loginService/login.service';
import {User} from '../../../models/user';
import {FileEntity} from '../../../models/fileEntity';
import {FileService} from '../../../services/fileService/file.service';

@Component({
  selector: 'app-organizationlist',
  templateUrl: './organizationlist.component.html',
  styleUrls: ['./organizationlist.component.scss']
})
export class OrganizationlistComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  modalData: {
    event: Organization;
  };
  currentUser: User;
  organization: Organization = new Organization();
  organizationForm: FormGroup;
  oborganizations$: Observable<Organization[]>;
  organizations: Organization[] = [];
  successMessage: string;
  newDepartment = new Department();
  departments: Department[] = [];
  messageType: string;
  loadingB = false;
  errorAddDepart = false;
  searchText: any;

  constructor(private formBuilder: FormBuilder,
              private modal: NgbModal,
              private userService: UserService,
              private organizationService: OrganizationService,
              private authenticationService: LoginService,
              private fileService: FileService) {
    this.currentUser = this.authenticationService.convertTokenToUser();
  }

  ngOnInit() {
    this.oborganizations$ = this.userService.getAllOrganizations();
    this.oborganizations$.subscribe((data) => {
      this.organizations = data;
    }, error => {
      this.showMessage('Kon organizaties niet ophalen', true);
    });
    this.organizationForm = this.formBuilder.group({
      image: [''],
      siteUrl: [''],
      email: [''],
      name: [''],
      description: [''],
      location: [''],
      phone: [''],
      newDepartmentInputName: [''],
      newDepartmentInputLocation: [''],
      newDepartmentInputDescription: [''],
    });

  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }

  get f() {
    return this.organizationForm.controls;
  }

  oModal(event: Organization): void {
    this.modalData = {event};
    this.newDepartment = new Department();
    this.modal.open(this.modalContent, {size: 'lg'});
  }

  addDepartment(organisation: Organization) {
    if (this.newDepartment.name !== undefined && this.newDepartment.name !== '' && this.newDepartment.location !== undefined && this.newDepartment.location !== '') {
      if (this.newDepartment.id !== undefined && this.newDepartment.id !== 0) {
        // update department
        organisation.departments[organisation.departments.findIndex(x => x.id === this.newDepartment.id)] = this.newDepartment;
        this.newDepartment = new Department();
      } else {
        // add department
        this.organizationService.addDepartmentToOrganisation(organisation.id, this.newDepartment).subscribe(data => {
          if (data === 200) {
            this.showMessage('De afdeling is alvast opgeslagen!');
            organisation.departments.push(this.newDepartment);
            this.newDepartment = new Department();
          } else {
            this.showMessage('Er is iets mis gegaan voor het opslaan van de afdeling, probeer het later nog eens.', true);
          }
        }, error => {
          this.showMessage('Er is iets mis gegaan voor het opslaan van de afdeling, probeer het later nog eens.', true);
          this.loadingB = false;
        });
      }
      this.errorAddDepart = false;
    } else {
      this.errorAddDepart = true;
    }
  }

  editDepartment(department: Department) {
    this.newDepartment = new Department();
    this.newDepartment = department;
    console.log(this.newDepartment);
  }

  removeDepartment(department: string, organization: Organization) {
    organization.departments.splice(organization.departments.findIndex(x => x.name === department), 1);
  }

  onSubmit(organization: Organization) {
    this.loadingB = true;
    this.userService.editOrganization(organization).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Organisatie is aangepast!');
        this.modal.dismissAll();
      } else {
        this.showMessage('Er is iets mis gegaan tijdens het aanpassen van de organisatie!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets mis gegaan tijdens het aanpassen van de organisatie!', true);
      this.loadingB = false;
    });
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  onFileChange(event, organization: Organization) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        organization.image = reader.result.toString();
      };
    }
  }

  removeFromList(organization: Organization) {
    const index: number = this.organizations.indexOf(organization);
    if (index !== -1) {
      this.organizations.splice(index, 1);
    }
  }

  delete(organization: Organization) {
    this.loadingB = true;
    this.organizationService.deleteOrganization(organization.id).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De organizatie is verwijderd');
        this.removeFromList(organization);
      } else {
        this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Kon organisatie niet verwijderen', true);
      this.loadingB = false;
    });
  }

}
