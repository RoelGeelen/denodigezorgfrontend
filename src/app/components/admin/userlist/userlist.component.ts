import {Observable} from 'rxjs';
import {UserService} from '../../../services/userService/user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Zzp} from 'src/app/models/zzp';
import {Role} from '../../../models/role';
import {Router} from '@angular/router';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  modalData: {
    event: Zzp;
  };
  zzpForm: FormGroup;
  obusers$: Observable<Zzp[]>;
  users: Zzp[] = [];
  successMessage: string;
  messageType: string;
  loadingB = false;

  constructor(private formBuilder: FormBuilder, private modal: NgbModal, private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.obusers$ = this.userService.getAllZzp();
    this.obusers$.subscribe((data) => {
      data.forEach((value) => {
        if (value.dateOfBirth) {
          value.dateOfBirth = value.dateOfBirth.split(' ', 2)[0];
        }
      });
      this.users = data;
      console.log(this.users);
    }, error => {
      this.showMessage('Kon gebruikers niet ophalen', true);
    });

    // this.zzp.account = this.account;

    this.zzpForm = this.formBuilder.group({
      id: [''],
      image: [''],
      email: [''],
      name: [''],
      gender: [''],
      age: [''],
      description: [''],
      education: [''],
      profession: [''],
      phone: [''],
    });
  }


  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  get f() {
    return this.zzpForm.controls;
  }

  oModal(event: Zzp): void {
    this.modalData = {event};
    this.modal.open(this.modalContent, {size: 'lg'});
  }

  onSubmit(zzp: Zzp) {
    this.loadingB = true;
    this.userService.editZzp(zzp).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Gebruiker is aangepast!');
        this.modal.dismissAll();
      } else {
        this.showMessage('Er is iets mis gegaan met het aanpasssen van de gebruiker!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets mis gegaan met het aanpasssen van de gebruiker!', true);
      this.loadingB = false;
    });
  }

  onFileChange(event, zzp: Zzp) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        zzp.account.image = reader.result.toString();
      };
    }
  }


  removeFromList(zzp: Zzp) {
    const index: number = this.users.indexOf(zzp);
    if (index !== -1) {
      this.users.splice(index, 1);
    }
  }

  delete(zzp: Zzp) {
    this.loadingB = true;
    this.userService.deleteUser(zzp.id).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Zorgprofessional verwijderd');
        this.removeFromList(zzp);
      } else {
        this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      this.loadingB = false;
    });
  }

  goToRegister() {
    this.router.navigate(['/register'], { queryParams: { role: Role.ZZP }});
  }
}
