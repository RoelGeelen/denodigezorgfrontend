import {Observable} from 'rxjs';
import {UserService} from '../../../services/userService/user.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Account} from '../../../models/account';
import {Router} from "@angular/router";
import {Role} from "../../../models/role";

@Component({
  selector: 'app-adminlist',
  templateUrl: './adminlist.component.html',
  styleUrls: ['./adminlist.component.scss']
})
export class AdminlistComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  modalData: {
    event: Account;
  };
  adminForm: FormGroup;
  obusers$: Observable<Account[]>;
  users: Account[] = [];
  successMessage: string;
  messageType: string;
  loadingB = false;
  searchText: any;

  constructor(private formBuilder: FormBuilder, private modal: NgbModal, private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.obusers$ = this.userService.getAllAdmin();
    this.obusers$.subscribe((data) => {
      this.users = data;
    }, error => {
      this.showMessage('Kon gebruikers niet ophalen', true);
    });

    // this.zzp.account = this.account;

    this.adminForm = this.formBuilder.group({
      image: [''],
      name: [''],
      email: [''],
      phone: [''],
    });
  }


  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  get f() {
    return this.adminForm.controls;
  }

  oModal(event: Account): void {
    this.modalData = {event};
    this.modal.open(this.modalContent, {size: 'lg'});
  }

  onSubmit(admin: Account) {
    this.loadingB = true;
    this.userService.editAdmin(admin).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Gebruiker is aangepast!');
        this.modal.dismissAll();
      } else {
        this.showMessage('Er is iets mis gegaan met het aanpasssen van de gebruiker!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets mis gegaan met het aanpasssen van de gebruiker!', true);
      this.loadingB = false;
    });
  }

  onFileChange(event, admin: Account) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        admin.image = reader.result.toString();
      };
    }
  }


  removeFromList(admin: Account) {
    const index: number = this.users.indexOf(admin);
    if (index !== -1) {
      this.users.splice(index, 1);
    }
  }

  delete(admin: Account) {
    this.loadingB = true;
    this.userService.deleteAdmin(admin.id).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Admin verwijderd');
        this.removeFromList(admin);
      } else {
        this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      this.loadingB = false;
    });
  }

  goToRegister() {
    this.router.navigate(['/register'], { queryParams: { role: Role.ADMIN }});
  }
}
