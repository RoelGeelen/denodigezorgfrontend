﻿import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {User} from '../../models/user';
import {Role} from '../../models/role';
import {NotificationMessage} from '../../models/notificationMessage';
import {NotificationService} from '../../services/notificationService/notification.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {FileService} from '../../services/fileService/file.service';
import {FileEntity} from '../../models/fileEntity';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  currentUser: User;
  // notificationCount = 0;
  // obNotifications$: Observable<NotificationMessage[]>;
  // notifications: NotificationMessage[] = [];
  // obFiles$: Observable<FileEntity[]>;
  // files: FileEntity[] = [];
  // filesOrg: FileEntity[] = [];
  successMessage: string;
  messageType: string;

  constructor(
    private modal: NgbModal,
    private authenticationService: LoginService
  ) {
    this.currentUser = this.authenticationService.convertTokenToUser();
  }

  ngOnInit() {
    // this.notificationService.getNotificationsAmount().subscribe((data) => {
    //     this.notificationCount = data;
    // });
    // this.obNotifications$ = this.notificationService.getNotifications();
    // this.obNotifications$.subscribe((data) => {
    //     data.forEach((value: NotificationMessage) => {
    //         value.date = new Date(value.date);
    //     });
    //     this.notifications = data;
    // }, error => {
    //     this.showMessage('Kon notificaties niet ophalen', true);
    // });
    // if (this.currentUser.role !== Role.ORGANIZATION) {
    //     this.obFiles$ = this.fileService.getFilesEmp();
    //     this.obFiles$.subscribe((data) => {
    //         this.files = data;
    //     }, error => {
    //         this.showMessage('Kon bestanden niet ophalen', true);
    //     });
    //     this.obFiles$ = this.fileService.getFilesOrg();
    //     this.obFiles$.subscribe((data) => {
    //         this.filesOrg = data;
    //     }, error => {
    //         this.showMessage('Kon bestanden niet ophalen', true);
    //     });
    // }

  }

  // open(file: FileEntity) {
  //     if (file.content) {
  //         const downloadLink = document.createElement('a');
  //         downloadLink.href = file.content;
  //         downloadLink.download = file.name + '.pdf';
  //         downloadLink.click();
  //     } else {
  //         this.showMessage('Geen bestand gevonden', true);
  //     }
  // }
  //
  // read(notification: NotificationMessage) {
  //     this.notificationService.setNotificationsRead(notification.id).subscribe((data) => {
  //         if (data === 200) {
  //             notification.isRead = true;
  //         }
  //     });
  // }
  // unread(notification: NotificationMessage) {
  //     this.notificationService.setNotificationsUnread(notification.id).subscribe((data) => {
  //         if (data === 200) {
  //             notification.isRead = false;
  //         }
  //     });
  // }
  //
  // get isOrganization() {
  //     return this.currentUser && this.currentUser.role === Role.ORGANIZATION;
  // }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }
}
