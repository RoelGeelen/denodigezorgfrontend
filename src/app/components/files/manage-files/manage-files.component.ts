import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FileEntity} from '../../../models/fileEntity';
import {FileService} from '../../../services/fileService/file.service';
import {Role} from '../../../models/role';
import {LoginService} from '../../../services/authenticationService/loginService/login.service';
import {User} from '../../../models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-manage-files',
  templateUrl: './manage-files.component.html',
  styleUrls: ['./manage-files.component.scss']
})
export class ManageFilesComponent implements OnInit {

  constructor(private fileService: FileService, private authenticationService: LoginService, private router: Router) {
    this.currentUser = authenticationService.convertTokenToUser();
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }
  currentUser: User;
  obfiles$: Observable<FileEntity[]>;
  files = new Map<string, FileEntity[]>();
  successMessage: string;
  messageType: string;
  loadingB = false;
  searchText: any;

  groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }

  ngOnInit() {
    this.obfiles$ = this.fileService.getFilesEmp();
    this.obfiles$.subscribe((data) => {
      data.forEach((value) => {
        value.uploadDate = new Date(value.uploadDate);
      });
      this.files = this.groupBy(data, file => file.header);
    }, error => {
      this.showMessage('Kon bestanden niet ophalen', true);
    });
  }

  open(file: FileEntity) {
    this.loadingB = true;
    if (file.content) {
      const downloadLink = document.createElement('a');
      downloadLink.href = file.content;
      downloadLink.download = file.filename;
      downloadLink.click();
      this.loadingB = false;
    } else {
      this.showMessage('Geen bestand gevonden', true);
      this.loadingB = false;
    }
  }


  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  removeFromList(file: FileEntity) {
    this.files.forEach((value) => {
      const index = value.indexOf(file);
      if (index !== -1) {
        value.splice(index, 1);
      }
    });
  }

  delete(file: FileEntity) {
    this.loadingB = true;
    this.fileService.deleteFile(file.id).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De bestand is verwijderd');
        this.removeFromList(file);
      } else {
        this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      }
      this.loadingB =  false;
    }, error => {
      this.showMessage('Er is iets is mis gegaan met het verwijderen!', true);
      this.loadingB = false;
    });
  }

  goToAddFile() {
    this.router.navigate(['/files/add'], { queryParams: { type: 'EMPLOYMENT'}});
  }
}
