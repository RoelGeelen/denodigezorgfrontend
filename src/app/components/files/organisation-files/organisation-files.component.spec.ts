import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationFilesComponent } from './organisation-files.component';

describe('OrganisationFilesComponent', () => {
  let component: OrganisationFilesComponent;
  let fixture: ComponentFixture<OrganisationFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
