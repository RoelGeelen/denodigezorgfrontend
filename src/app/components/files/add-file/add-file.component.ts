import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FileEntity} from '../../../models/fileEntity';
import {FileService} from '../../../services/fileService/file.service';
import {Observable} from 'rxjs';
import {Organization} from '../../../models/organization';
import {OrganizationService} from '../../../services/organizationService/organization.service';
import {ActivatedRoute} from "@angular/router";
import {Role} from "../../../models/role";

@Component({
  selector: 'app-add-file',
  templateUrl: './add-file.component.html',
  styleUrls: ['./add-file.component.scss']
})
export class AddFileComponent implements OnInit {
  successMessage: string;
  createFileForm: FormGroup;
  activeFile = new FileEntity();
  submitted = false;
  error = '';
  messageType: string;
  loadingB = false;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private fileService: FileService, private organizationService: OrganizationService) {
  }

  ngOnInit() {
    if (this.route.snapshot.queryParams.type) {
      if (this.route.snapshot.queryParams.type === 'EMPLOYMENT' || this.route.snapshot.queryParams.type === 'INFO') {
        this.activeFile.fileType = this.route.snapshot.queryParams.type;
      }
    }
    this.createFileForm = this.formBuilder.group({
      organization: ['', Validators.required],
      name: ['', Validators.required],
      content: ['', Validators.required],
      filetype: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.createFileForm.controls;
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  fileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.activeFile.filename = file.name;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.activeFile.content = reader.result.toString();
      };
    }
  }

  onSubmit() {
    this.submitted = true;
    this.loadingB = true;
    this.activeFile.uploadDate = new Date();
    // stop here if form is invalid
    if (this.createFileForm.invalid) {
      this.loadingB = false;
      return;
    }
    console.log(this.activeFile);
    this.fileService.addFile(this.activeFile).subscribe((data) => {
      if (data === 200) {
        this.showMessage('Het bestand succesvol toegevoegd.');
        this.submitted = false;
        this.activeFile = new FileEntity();
        this.createFileForm.reset();
      } else {
        this.showMessage('Er is iets mis gegaan bij het uploaden van het bestand!', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('Er is iets mis gegaan bij het uploaden van het bestand!');
      this.loadingB = false;
    });
  }
}
