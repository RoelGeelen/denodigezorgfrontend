import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PasswordService} from '../../services/authenticationService/passwordService/password.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  submitted = false;
  loading = false;
  successMessage: string;
  messageType: string;

  constructor(private formBuilder: FormBuilder, private passwordService: PasswordService, private router: Router) { }

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', Validators.required]
    });
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
    })();
  }

  get f() { return this.forgotPasswordForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.loading = true;
    this.passwordService.forgotPassword(this.f.email.value).subscribe(data => {
        this.loading = false;
        if (data === 200) {
          this.router.navigate(['/login'], { queryParams: { message: 'De email is succesvol naar uw mail adress verstuurd.' } });
        } else {
          this.showMessage('Er is iets fout gegaan, probeer het later opnieuw of neem contact met ons op.', true);
        }
      }, error => {
        this.showMessage('Er is iets fout gegaan, probeer het later opnieuw of neem contact met ons op.', true);
        this.loading = false;
      }
    );
  }

}
