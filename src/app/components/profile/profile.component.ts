import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {User} from '../../models/user';
import {Role} from '../../models/role';
import {Observable} from 'rxjs';
import {ProfileService} from '../../services/profileService/profile.service';
import {Zzp} from '../../models/zzp';
import {Gender} from '../../models/gender';
import {EducationLevel} from '../../models/educationLevel';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  obUser$: Observable<any>;
  user: any = new Zzp();
  messageType: string;
  successMessage: string;
  currentUser: User;

  constructor(
    private authenticationService: LoginService,
    private profileService: ProfileService
  ) {
    this.currentUser = authenticationService.convertTokenToUser();
  }

  ngOnInit() {
    this.getUser();
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  getUser() {
    this.obUser$ = this.profileService.getProfile();
    this.obUser$.subscribe((data) => {
      this.user = data;
      if (this.user.dateOfBirth) {
        this.user.dateOfBirth = this.user.dateOfBirth.split(' ', 2)[0];
      }
      this.user.gender = Gender[this.user.gender];
      this.user.educationLevel = EducationLevel[this.user.educationLevel];
    }, error => {
      this.showMessage('Kon profiel niet ophalen', true);
    });
  }

  get isZzp() {
    return this.currentUser && this.currentUser.role === Role.ZZP;
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }
}
