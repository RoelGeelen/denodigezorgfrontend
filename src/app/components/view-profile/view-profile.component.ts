import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../services/profileService/profile.service';
import {Zzp} from '../../models/zzp';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.scss']
})
export class ViewProfileComponent implements OnInit {
  obZzp$: Observable<Zzp>;
  zzp: Zzp;

  constructor(private route: ActivatedRoute, private profileService: ProfileService) { }

  ngOnInit() {
    this.obZzp$ = this.profileService.getZzpProfile(+this.route.snapshot.paramMap.get('id'));
    this.obZzp$.subscribe((data) => {
      this.zzp = data;
    });
  }

}
