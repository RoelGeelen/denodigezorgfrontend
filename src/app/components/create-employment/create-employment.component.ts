import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Employment} from '../../models/employment';
import {Observable, Subject} from 'rxjs';
import {EducationLevel} from '../../models/educationLevel';
import {EmploymentService} from 'src/app/services/employmentService/employment.service';
import {Role} from '../../models/role';
import {User} from '../../models/user';
import {LoginService} from '../../services/authenticationService/loginService/login.service';
import {Organization} from '../../models/organization';
import {OrganizationService} from '../../services/organizationService/organization.service';
import {UserService} from '../../services/userService/user.service';
import {Zzp} from '../../models/zzp';
import {Department} from '../../models/department';
import {DaterangepickerDirective, LocaleConfig} from 'ngx-daterangepicker-material';
import * as moment from 'moment';
import * as localization from 'moment/locale/nl';
moment.locale('nl', localization);


@Component({
  selector: 'app-create-employment',
  templateUrl: './create-employment.component.html',
  styleUrls: ['./create-employment.component.scss'],
})

export class CreateEmploymentComponent implements OnInit {


  constructor(
    private formBuilder: FormBuilder,
    private employmentService: EmploymentService,
    private authenticationService: LoginService,
    private organizationService: OrganizationService,
    private userService: UserService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.createEmploymentForm.controls;
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.ADMIN;
  }
  @ViewChild(DaterangepickerDirective, {  })
  daterangepicker: DaterangepickerDirective;
  locale: LocaleConfig = {
    format: 'YYYY-MM-DDTHH:mm:ss.SSSSZ',
    displayFormat: 'DD MMMM YYYY HH:mm',
    separator: ' Tot ',
    cancelLabel: 'Annuleer',
    applyLabel: 'Oke',
    customRangeLabel: ' - ',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek(),
  };
  opens = 'right';
  drops = 'up';
  dateTimeRange = { startDate: moment(), endDate: moment() };


  private success = new Subject<string>();
  successMessage: string;
  messageType: string;
  currentUser: User = new User();
  loadingB = false;
  organizationPlaceholder = 'Ingelogde organisatie';
  educationLevelsPlaceholder$: string[] = ['2', '2+', '3', '4', '5', '6'];

  dateTimeRangeError = '';

  selectedEducationLevel = '2';

  createEmploymentForm: FormGroup;
  activeEmployment = new Employment();
  submitted = false;
  error = '';

  obOrganization$: Observable<Organization[]>;
  organizations: Organization[] = [];
  users: Zzp[] = [];
  zzps: Zzp[] = [];

  departments: Department[];
  selectedOrganization: Organization;


  ngOnInit() {
    this.createEmploymentForm = this.formBuilder.group({
      zzper: ['', Validators.required],
      organization: ['', Validators.required],
      description: [''],
      function: ['', null],
      department: ['', Validators.required],
      educationLevel: [null],
      extraInfo: [''],
      location: ['', Validators.required],
      rangeDate: [null],
      plannedStartDate: [null],
      plannedEndDate: [null]
    });
    this.obOrganization$ = this.organizationService.getOrganizations();
    this.obOrganization$.subscribe((data) => {
      this.organizations = data;
    }, error => {
      this.showMessage('Kon organisaties niet ophalen', true);
    });
    this.userService.getAllZzp().subscribe((data) => {
      this.users = data;
    }, error => {
      this.showMessage('Kon zzpers niet ophalen', true);
    });

  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  showMessage(message: string, hasFailed?: boolean) {
    (async () => {
      hasFailed ? this.messageType = 'danger' : this.messageType = 'success';
      this.successMessage = message;
      await this.delay(5000);
      this.successMessage = null;
    })();
  }

  dataChanged(newObj) {
    if (newObj) {
      this.selectedOrganization = newObj;
      this.departments = this.selectedOrganization.departments;
    }
  }

  departmentChanged(department: Department) {
    if (department) {
      this.activeEmployment.location = department.location;
    }
  }

  selectUser(zzp) {
    if (zzp) {
      this.zzps = [zzp];
      this.activeEmployment.zzp = this.zzps;
    }
  }

  onSubmit() {
    this.submitted = true;
    this.loadingB = true;
    this.dateTimeRangeError = '';
    console.log(this.dateTimeRange.startDate.toDate());
    if (this.dateTimeRange.startDate == null || this.dateTimeRange.endDate == null) {
      this.dateTimeRangeError = 'Start of eindtijd niet geselecteerd';
      this.loadingB = false;
      return;
    }

    this.activeEmployment.plannedStartDate = this.dateTimeRange.startDate.toDate();
    this.activeEmployment.plannedEndDate = this.dateTimeRange.endDate.toDate();

    switch (this.selectedEducationLevel) {
      case '2': {
        this.activeEmployment.educationLevel = EducationLevel.TWO;
        break;
      }
      case '2+': {
        this.activeEmployment.educationLevel = EducationLevel.TWO_PLUS;
        break;
      }
      case '3': {
        this.activeEmployment.educationLevel = EducationLevel.THREE;
        break;
      }
      case '4': {
        this.activeEmployment.educationLevel = EducationLevel.FOUR;
        break;
      }
      case '5': {
        this.activeEmployment.educationLevel = EducationLevel.FIVE;
        break;
      }
      case '6': {
        this.activeEmployment.educationLevel = EducationLevel.SIX;
        break;
      }
    }

    // stop here if form is invalid
    if (this.createEmploymentForm.invalid) {
      this.loadingB = false;
      return;
    }
    this.employmentService.addEmployment(this.activeEmployment).subscribe((data) => {
      if (data === 200) {
        this.showMessage('De dienst is succesvol aangemaakt.');
        this.submitted = false;
        this.createEmploymentForm.reset();
        this.dateTimeRange = { startDate: moment(), endDate: moment() };
      } else {
        this.showMessage('De dienst is niet aangemaakt.', true);
      }
      this.loadingB = false;
    }, error => {
      this.showMessage('De dienst is niet aangemaakt.', true);
      this.loadingB = false;
    });
  }
}
